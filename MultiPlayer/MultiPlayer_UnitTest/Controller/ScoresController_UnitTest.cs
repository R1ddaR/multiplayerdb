﻿using Microsoft.EntityFrameworkCore;
using Moq;
using MultiPlayer.Controllers;
using MultiPlayer.Data;
using MultiPlayer.Models;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace MultiPlayer_UnitTest.Controller
{
    public class ScoresController_UnitTest
    {


        [Fact]
        public void GetScores_ShouldValidateWithCorrectParameters()
        {
            //Arrange
            var mockService = new Mock<IScoresRepository<Score>>();
            var scores = new ScoresController(mockService.Object);
            //act
            var actual = scores.GetScores();
            //Assert
            mockService.Verify(m => m.GetScores(), Times.Once());
        }

        [Fact]
        public void GetScoreById_ShouldValidateWithCorrectParameters()
        {
            //Arrange
            var mockService = new Mock<IScoresRepository<Score>>();
            var scores = new ScoresController(mockService.Object);
            //act
            int id = 1;
            scores.GetScoreById(id);
            //Assert
            mockService.Verify(m => m.GetScoreById(id), Times.Once());
        }

        [Fact]
        public void CreateScore_ShouldValidateWithCorrectParameters()
        {
            int i = 1;
            string u = "test";
            int d = 1;

            Score score = new Score()
            {
                Id = i,
                GameId = i,
                UserId = u,
                TeamId = i,
                Wins = d,
                Losses = d,
                Points = d
            };

            //Arrange
            var mockService = new Mock<IScoresRepository<Score>>();
            var scores = new ScoresController(mockService.Object);
            //act
            scores.CreateScore(score);
            mockService.Setup(m => m.CreateScore(score));
            //Assert
            mockService.Verify(m => m.CreateScore(score), Times.Once());
        }

        [Fact]
        public void UpdateScoreById_ShouldValidateWithCorrectParameters()
        {
            int i = 1;
            string u = "test";
            int d = 1;

            Score score = new Score()
            {
                Id = i,
                GameId = i,
                UserId = u,
                TeamId = i,
                Wins = d,
                Losses = d,
                Points = d
            };

            //Arrange
            var mockService = new Mock<IScoresRepository<Score>>();
            var scores = new ScoresController(mockService.Object);
            //act
            var actual = scores.UpdateScoreById(score);
            var expected = mockService.Setup(m => m.UpdateScore(score));
            //Assert
            mockService.Verify(m => m.UpdateScore(score), Times.Once());
        }

        [Fact]
        public void DeleteScoreById_ShouldValidateWithCorrectParameters()
        {
            int i = 1;
            string u = "test";
            int d = 1;

            Score score = new Score()
            {
                Id = i,
                GameId = i,
                UserId = u,
                TeamId = i,
                Wins = d,
                Losses = d,
                Points = d
            };

            //Arrange
            var mockService = new Mock<IScoresRepository<Score>>();
            var scores = new ScoresController(mockService.Object);
            //act
            scores.DeleteScore(score);
            //Assert
            mockService.Verify(m => m.DeleteScore(score), Times.Once());
        }

        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        [InlineData(3)]
        public void GetWinsByGameId_ShouldValidateWithCorrectParameters(int gameId)
        {
            //arrange
            var mockService = new Mock<IScoresRepository<Score>>();
            var scores = new ScoresController(mockService.Object);
            //act
            scores.GetWinsByGameId(gameId);
            //Assert
            mockService.Verify(m => m.GetWinsByGameId(gameId), Times.Once());

        }

        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        [InlineData(3)]
        public void GetWinsByGameId_shouldreturnAnyWinsIfValid(int gameId)
        {
            //arrange
            var mockService = new Mock<IScoresRepository<Score>>();
            //act
            var score = mockService.Object.GetWinsByGameId(gameId);
            //assert
            mockService.Verify(s => s.GetWinsByGameId(It.IsAny<int>()), Times.Once());
        }

        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        [InlineData(3)]
        public void GetLossesByGameId_ShouldValidateWithCorrectParameters(int gameId)
        {
            //arrange
            var mockService = new Mock<IScoresRepository<Score>>();
            var scores = new ScoresController(mockService.Object);
            //act
            scores.GetLossesByGameId(gameId);
            //Assert
            mockService.Verify(m => m.GetLossesByGameId(gameId), Times.Once());
        }
    }
}
