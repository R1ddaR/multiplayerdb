﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MultiPlayer.Models
{
    public class Game
    {
        [Key]
        public int Id { get; set; }
        [StringLength(50)]
        public string Name { get; set; }
        public bool FriendlyFire { get; set; }

        public virtual IList<Team> Teams { get; set; }
        public virtual IList<Score> Scores { get; set; }
    }
}
