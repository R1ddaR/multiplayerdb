﻿using MultiPlayer.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MultiPlayer.Models
{

    
    public class Score
    {
        [Key]
        public int Id { get; set; }
        public double Points { get; set; }
        public double Wins { get; set; }
        public double Losses { get; set; }
        [ForeignKey("TeamId")]
        public virtual Team Team { get; set; }
        public int TeamId { get; set; }
        [ForeignKey("UserId")]
        public virtual ApplicationUser User { get; set; }
        public string UserId { get; set; }
        [ForeignKey("GameId")]
        public virtual Game Game { get; set; }
        public int GameId { get; set; }
    }

    




    
}
