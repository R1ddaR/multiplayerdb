﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MultiPlayer.Models
{
    public class Team
    {
        [Key]
        public int Id { get; set; }
        [StringLength(50)]
        public string Name { get; set; }

        public virtual IList<ApplicationUser> Users { get; set; }
        public virtual IList<Score> Scores { get; set; }
        [ForeignKey("GameId")]
        public virtual Game Game { get; set; }
        public int GameId { get; set; }
    }
}
