﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using MultiPlayer.Models;

namespace MultiPlayer.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options): base(options) { }

        public DbSet<Score> Scores { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<Game> Games { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<Team>().HasKey(t => t.Id);
            builder.Entity<Team>().HasOne(t => t.Game).WithMany(g => g.Teams);
            builder.Entity<Team>().HasMany(t => t.Users);

            builder.Entity<Game>().HasKey(g => g.Id);
            builder.Entity<Game>().HasMany(g => g.Teams).WithOne(t => t.Game);
        }
    }
}
