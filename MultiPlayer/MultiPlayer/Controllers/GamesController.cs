﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MultiPlayer.Data;
using MultiPlayer.Models;

namespace MultiPlayer.Controllers
{
    [Produces("application/json")]
    [Route("api/Games")]
    public class GamesController : Controller
    {
        private readonly IGamesRepository<Game> service;

        public GamesController(IGamesRepository<Game> _service)
        {
            service = _service;
        }

        // GET: api/Games
        [HttpGet]
        public IEnumerable<Game> GetGames()
        {
            return service.GetGames();
        }

        // GET: api/Games/5
        [HttpGet("{id}")]
        public Game GetGame([FromRoute] int id)
        {
            var game = service.Games.Where(g => g.Id == id).SingleOrDefault();

            if (game == null)
                return null;

            return service.GetGame(id);
        }

        [HttpGet("CreateGame")]
        public Game CreateGame(string name, bool friendlyFire, IList<Team> teams)
        {
            Game game = new Game()
            {
                Name = name,
                FriendlyFire = friendlyFire,
                Teams = teams
            };

            return game;
        }

        [HttpGet("UpdateGame")]
        public Game UpdateGame(int id, string name, bool friendlyFire, IList<Team> teams, IList<Score> scores)
        {
            var game = GetGame(id);
            game.Name = name;
            game.FriendlyFire = friendlyFire;
            game.Teams = teams;
            game.Scores = scores;

            return game;
        }

        [HttpGet("DeleteGame")]
        public Game DeleteGame(int id)
        {
            var game = GetGame(id);
            return game;
        }
    }
}