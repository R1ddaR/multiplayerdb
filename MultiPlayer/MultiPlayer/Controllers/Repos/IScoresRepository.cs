﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using MultiPlayer.Data;
using MultiPlayer.Models;
using System.Linq;
using System;

namespace MultiPlayer.Controllers
{
    public interface IScoresRepository<T>
    {
        IEnumerable<T> GetScores();
        T GetScoreById(int id);
        void CreateScore(T score);
        void UpdateScore(T score);
        void DeleteScore(T score);
        double GetLossesByGameId(int gameId);
        double GetWinsByGameId(int gameId);
    }

    public class FakeScoresRepository : IScoresRepository<Score>
    {
        private List<Score> scores = new List<Score>();
        private List<Game> games = new List<Game>();

        private FakeScoresRepository()
        {
            scores.Add(new Score() { Id = 1, Losses = 1, Wins = 1, Points = 1, GameId = 1, TeamId = 1, UserId = "1" });
            scores.Add(new Score() { Id = 2, Losses = 1, Wins = 1, Points = 1, GameId = 1, TeamId = 1, UserId = "1" });
            scores.Add(new Score() { Id = 3, Losses = 1, Wins = 1, Points = 1, GameId = 1, TeamId = 1, UserId = "2" });

            games.Add(new Game() { Id = 1, FriendlyFire = true, Name = "test" });
        }

        public IEnumerable<Score> GetScores()
        {
            var tempScores = scores;

            if (tempScores == null)
                throw new Exception("No scores found!");

            return scores;
        }

        public Score GetScoreById(int id)
        {
            var score = scores.Where(s => s.Id == id).FirstOrDefault();

            if (score == null)
                throw new Exception("could not find score!");

            return score;
        }

        public void CreateScore(Score score)
        {

            if (score == null)
                throw new Exception("no score Detected!");

            scores.Add(score);
        }

        public void DeleteScore(Score score)
        {
            if (score == null)
                throw new Exception("no score Detected!");

            scores.Remove(score);
        }

        public void UpdateScore(Score score)
        {         
            var tempScore = scores.Where(s => s.Id == score.Id).FirstOrDefault();

            if (tempScore == null)
               throw new Exception("could not find that score!");

            scores.Remove(tempScore);
            scores.Add(score);
        }


        public double GetWinsByGameId(int gameId)
        {
            var game = games.Where(g => g.Id == gameId).FirstOrDefault();

            if (game == null)
                throw new Exception("could not find that game!");

            double wins = 0;

            foreach (var score in game.Scores)
            {
                wins += score.Wins;
            }

            return wins;
        }

        public double GetLossesByGameId(int gameId)
        {
            var game = games.Where(g => g.Id == gameId).FirstOrDefault();

            if (game == null)
                throw new Exception("could not find that game!");

            double losses = 0;

            foreach (var score in game.Scores)
            {
                losses += score.Losses;
            }

            return losses;
        }

    }
}