﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using MultiPlayer.Models;

namespace MultiPlayer.Controllers
{
    public interface IGamesRepository<T>
    {
        IEnumerable<Game> Games { get; set; }

        T CreateGame(string name, bool friendlyFire, IList<Team> teams);
        T DeleteGame(int id);
        T GetGame([FromRoute] int id);
        IEnumerable<T> GetGames();
        T UpdateGame(int id, string name, bool friendlyFire, IList<Team> teams, IList<Score> scores);
    }
}