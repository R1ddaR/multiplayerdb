﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MultiPlayer.Data;
using MultiPlayer.Models;
using MultiPlayer.Services;

namespace MultiPlayer.Controllers
{
    [Produces("application/json")]
    [Route("api/Scores")]
    public class ScoresController : Controller
    {
        private readonly IScoresRepository<Score> service;

        public ScoresController(IScoresRepository<Score> _service)
        {
            service = _service;
        }

        [HttpGet]
        public IEnumerable<Score> GetScores()
        {
            return service.GetScores();
        }

        [HttpGet("{Id}")]
        public Score GetScoreById([FromRoute] int id)
        {
            return service.GetScoreById(id);
        }

        [HttpGet("CreateScore")]
        public Score CreateScore(Score score)
        {

            //Score score = new Score()
            //{
            //    GameId = gameId,
            //    UserId = userId,
            //    TeamId = teamId,
            //    Wins = wins,
            //    Losses = losses,
            //    Points = points
            //};

            service.CreateScore(score);

            return score;
        }

        public Score UpdateScoreById(Score score)
        {
            if (GetScoreById(score.Id) != null && score == null)
                return null;

            service.UpdateScore(score);

            return score;
        }

        [HttpGet("DeleteScore")]
        public Score DeleteScore(Score score)
        {

            service.DeleteScore(score);

            return score;
        }

        [HttpGet("GetLossesByGameId")]
        public double GetLossesByGameId(int gameId)
        {
            var losses = service.GetLossesByGameId(gameId);
            return losses;
        }

        [HttpGet("GetWinsByGameId")]
        public double GetWinsByGameId(int gameId)
        {
            var wins = service.GetWinsByGameId(gameId);
            return wins;

        }
    }
}