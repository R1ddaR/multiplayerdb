import * as React from 'react';
import { Route } from 'react-router-dom';
import { Layout } from './components/Layout';
import { Home } from './components/Home';
import { FetchData } from './components/FetchData';
import { Counter } from './components/Counter';
import { CreateGame } from './components/CreateGame';
import { JoinGame } from './components/JoinGame';


export const routes = <Layout>
    <Route exact path='/' component={ Home } />
    <Route path='/counter' component={ Counter } />
    <Route path='/fetchdata' component={ FetchData } />
    <Route path='/CreateGame' component={CreateGame} />
    <Route path='/JoinGame' component={JoinGame} />
</Layout>;
