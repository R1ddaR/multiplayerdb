import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { Link, NavLink } from 'react-router-dom';


class Team {
    Id: number;
    Name: string;
}

class Game {
    Id: number;
    Name: string;
    FriendlyFire: boolean;
    Teams: Team[]
}

interface ICreateGame {
    Games: Game[];
    loading: boolean;
}

export class CreateGame extends React.Component<RouteComponentProps<{}>, ICreateGame> {
    constructor() {
        super();
        this.state = {
            Games: [],
            loading: true
        };

        this.FetchGames = this.FetchGames.bind(this);
    }

    public render() {
        var divStyle = {
            width: '40rem'
        }

        var friendlyFireChecked = true;

        return <div className="card" style={divStyle}>
            <img className="card-img-top" src="/Images/ScoreBoard.png" alt="Card image cap" />
            <div className="card-body">
                <h5 className="card-title">MultiPlayer - Create Game</h5>
                <p className="card-text">
                    <ul>
                        GameName: <input type="text" />
                    </ul>
                    <ul>
                        FriendlyFire: <input type="checkbox" defaultChecked={friendlyFireChecked} />
                    </ul>
                    <ul>
                        <table>
                            <thead>
                                <th>Teams</th>
                            </thead>
                            <tbody>
                                <div id="Teams" className="col-md-12"></div>
                                <button onClick={this.AddTeam}>AddTeam</button>
                            </tbody>
                        </table>
                    </ul>
                </p>
            </div>
            <NavLink to={'/'} exact activeClassName='active' className='btn btn-primary'>Create Game</NavLink>
        </div>;
    }

    public AddTeam() {
        let newTeams = "<li>Team:<input typeText Id='AddMoreTeams' name='Teams'/></li>";
        (document.getElementById("Teams") as HTMLDivElement).innerHTML += newTeams;
    }

    public FetchGames() {
        fetch("api/Scores")
            .then(response => response.json() as Promise<Game[]>)
            .then(data => {
                console.log(data);
                this.setState({ Games: data, loading: false });
            })
            .catch(error => { console.log("error: ", error) });
    }

    componentDidMount() {
        this.FetchGames();
    }
}
