import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { Link, NavLink } from 'react-router-dom';

class Score {
    id: number;
    points: number;
    wins: number;
    losses: number;
    gameId: number;
    teamId: number;
}

interface IScoreBoard {
    Scores: Score[];
    loading: boolean;
}


export class Home extends React.Component<RouteComponentProps<{}>, IScoreBoard> {
    constructor() {
        super();
        this.state = {
            Scores: [],
            loading: true
        };

        this.FetchScores = this.FetchScores.bind(this);
    }

    public render() {
        let content = this.state.loading
            ? <p><em>Loading...</em></p>
            : this.renderScoreBoardTable(this.state.Scores);

        var divStyle = {
            width: '40rem'
        }

        return <div className="card" style={divStyle}>
            <img className="card-img-top" src="/Images/ScoreBoard.png" alt="Card image cap" />
            <div className="card-body">
                <h5 className="card-title">MultiPlayer - ScoreBoard</h5>
                <div className="card-text">{content}</div>
            </div>
            <NavLink to={'/CreateGame'} exact activeClassName='active' className='btn btn-primary'>Create Game</NavLink>
            <NavLink to={'/JoinGame'} exact activeClassName='active' className='btn btn-primary'>Join Game</NavLink>
        </div>;
    }

    public renderScoreBoardTable(scores: Score[]) {
        return <table className="table">
            <thead>
                <tr>
                    <th>Game</th>
                    <th>team</th>
                    <th>Wins/losses</th>
                    <th>WinningTeams</th>
                    <th>LoosingTeams</th>
                    <th>Points</th>
                </tr>
            </thead>
            <tbody>
                {scores.map(s =>
                    <tr key={s.id}>
                        <td>{s.id}</td>
                        <td>{s.teamId}</td>
                        <td>{s.wins}/{s.losses}</td>
                        <td>WinningTeam</td>
                        <td>LoosingTeam</td>
                        <td>{s.points}</td>
                    </tr>
                )}
            </tbody>
        </table>
    }

    public FetchScores() {
        fetch("api/Scores")
            .then(response => response.json() as Promise<Score[]>)
            .then(data => {
                console.log(data);
                this.setState({ Scores: data, loading: false });
            })
            .catch(error => { console.log("error: ", error) });
    }

    componentDidMount() {
        this.FetchScores();
    }
}
