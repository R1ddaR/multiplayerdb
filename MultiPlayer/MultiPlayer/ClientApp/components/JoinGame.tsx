import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import { Link, NavLink } from 'react-router-dom';


class Team {
    id: number;
    name: string;
}

class Game {
    id: number;
    name: string;
    friendlyFire: boolean;
    teams: Team[]
}

interface IJoinGame {
    Games: Game[];
    Teams: Team[];
    loading: boolean;
}

export class JoinGame extends React.Component<RouteComponentProps<{}>, IJoinGame> {
    constructor() {
        super();
        this.state = {
            Games: [],
            Teams: [],
            loading: true
        };

        this.FetchGames = this.FetchGames.bind(this);
    }

    public render() {
        var friendlyFireChecked = true;

        let content = this.state.loading
            ? <p><em>Loading...</em></p>
            : this.renderJoinGameTable(this.state.Games);

        return <div className="card" style={{width: '40rem'}}>
            <img className="card-img-top" src="/Images/ScoreBoard.png" alt="Card image cap" />
            <div className="card-body">
                <h5 className="card-title">MultiPlayer - Join Game</h5>
                <p className="card-text">
                    {content}
                </p>
            </div>

        </div>;
    }

    public renderJoinGameTable(games: Game[]) {
        return <table className="table">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Teams</th>
                    <th>FriendlyFire</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                {games.map(s =>
                    <tr key={s.id}>
                        <td>{s.id}</td>
                        <td>{s.name}</td>
                        {this.FetchTeams(s.id)}
                        <td><input type="checkbox" checked={s.friendlyFire} /></td>
                        <NavLink to={'/'} exact activeClassName='active' className='btn btn-primary'>Join Game</NavLink>
                    </tr>
                )}
            </tbody>
        </table>
    }

    public AddTeam() {
        let newTeams = "<li>Team:<input typeText Id='AddMoreTeams' name='Teams'/></li>";
        (document.getElementById("Teams") as HTMLDivElement).innerHTML += newTeams;
    }

    public FetchGames() {
        fetch("api/Games")
            .then(response => response.json() as Promise<Game[]>)
            .then(data => {
                console.log(data);
                this.setState({ Games: data, loading: false });
            })
            .catch(error => { console.log("error: ", error) });
    }

    public FetchTeams(GameId: number) {
        fetch("api/Teams")
            .then(response => response.json() as Promise<Game[]>)
            .then(data => {
                console.log(data);
                this.setState({ Teams: data, loading: false });
            })
            .catch(error => { console.log("error: ", error) });

        return <td>
            {}
        </td>
    }

    componentDidMount() {
        this.FetchGames();
    }
}
